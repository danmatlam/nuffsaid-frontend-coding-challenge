import React, { useState } from 'react';

import { Box, IconButton, Typography } from '@material-ui/core';
import CloseRoundedIcon from '@material-ui/icons/CloseRounded';
import styled from 'styled-components';



const NotiIttem = ({ color, title, handleRemove }) => {

    const [opacity, setOpacity] = useState(0);

    setTimeout(() => {
        setOpacity(100);
    }, 100);


    const onRemove = ()=>{
        handleRemove()
    }
    return (
        <View color={color} opacity={opacity} data-testid="noti-item">
            <Typography variant="body1">{title}</Typography>
            <Box>
                <IconButton onClick={handleRemove}>
                    <CloseRoundedIcon></CloseRoundedIcon>
                </IconButton>
            </Box>
        </View>

    )
}

export default NotiIttem


const View = styled.div`
    background-color: ${props => props.color};
    height: 4.5em;
    border-radius:9pt;
    padding: .3em;
    margin-bottom: .6em;



    opacity: ${props => props.opacity};
    will-change: transform;
    transition: .3s ease;


    display: flex;

    justify-content: space-between;
    flex:1;
    width:100%;

`

