import { Badge, CardHeader, Typography } from '@material-ui/core';
import React from 'react'
import JSONTree from 'react-json-tree';
import styled from 'styled-components';
import NotificationsIcon from '@material-ui/icons/Notifications';
import NotiIttem from './NotiIttem';
import { MEDIA_SCREENS } from '../../theme/breakPoints'
const NotiBoard = ({
    errorArr,
    infoArr,
    warningArr,
    handleRemove

}) => {
    return (
        <View  data-testid="noti-board">

            <ViewColumn>
                <Header>
                    <Typography variant="h6">Error Type 3</Typography>
                    <Badge badgeContent={errorArr.length} color="error" ><NotificationsIcon /> </Badge>
                </Header>
                {errorArr.map(item =>
                    <NotiIttem
                        key={item.message}
                        color={"#F56236"}
                        title={item.message}
                        handleRemove={() => handleRemove(1, item.message)}
                    />)}
            </ViewColumn>

            <ViewColumn>
                <Header>
                    <Typography variant="h6">Error type 2</Typography>
                    <Badge badgeContent={warningArr.length} color="error"><NotificationsIcon /> </Badge>
                </Header>
                {warningArr.map(item =>
                    <NotiIttem
                        key={item.message}
                        color={"#FCE788"}
                        title={item.message}
                        handleRemove={() => handleRemove(2, item.message)}
                    />)}
            </ViewColumn>

            <ViewColumn>
                <Header>
                    <Typography variant="h6">Error type 1</Typography>
                    <Badge badgeContent={infoArr.length} color="error"><NotificationsIcon /> </Badge>
                </Header>
                {infoArr.map(item =>
                    <NotiIttem
                        key={item.message}
                        color={"#88FCA3"}
                        title={item.message}
                        handleRemove={() => handleRemove(3, item.message)}
                    />
                )}
            </ViewColumn>


        </View>
    )
}

export default NotiBoard;



const View = styled.div`
    display: flex;
    justify-content: space-between;
    background-color: #E9EBEE;


    @media only screen and (min-width: ${MEDIA_SCREENS.XS.FROM}px) and (max-width: ${MEDIA_SCREENS.XS.TO}px)  {
       flex-direction: column;
    }
    @media only screen and (min-width: ${MEDIA_SCREENS.SMALL.FROM}px) and (max-width: ${MEDIA_SCREENS.SMALL.TO}px)  {
        flex-direction: column;

    }
    @media only screen and (min-width: ${MEDIA_SCREENS.MEDIUM.FROM}px)   {
        flex-direction: row;

    }

`;

const ViewColumn = styled.div`
    padding:.9em;
    border-radius: 9pt;
    height: 60vh;
    overflow-x:scroll;
    background-color: #ffffff;
    flex:1;
    margin: .3em;

`


const Header = styled.div`
    display: flex;
    flex-direction: row;
    justify-content:space-between;
    align-items: center;
    margin:.3em .3em .9em .3em;
    
    
`;
