import React, { useState, useEffect } from 'react'
import Api from '../api';

import NotiBoard from './NotiBoard';
import { Button } from '@material-ui/core';
import styled from 'styled-components';
import { toast } from 'react-toastify';

const MessageList = () => {



    const [messageArr, setMessageArr] = useState([]);
    const [message, setMessage] = useState({});


    const [errorArr, setErrorArr] = useState([]);
    const [warningArr, setWarningArr] = useState([]);
    const [infoArr, setInfoArr] = useState([]);
    const [toggle, setToggle] = useState(false);

    const [api, setApi] = useState(
        new Api({
            messageCallback: (message) => {
                if (message && message.priority) {
                    setMessage(message);
                }
            },
        })
    );






    const handleStart = () => {
        api.start();
        setToggle(true)
    }

    const handleStop = () => {
        api.stop();
        setToggle(false)
    }

    const handleClear = () => {
        setErrorArr([]);
        setWarningArr([]);
        setInfoArr([]);
    }




    const handleRemove = (severity, message) => {
        if (severity == 1) {
            setErrorArr(errorArr.filter(item => item.message !== message));
        }
        if (severity == 2) {
            setWarningArr(warningArr.filter(item => item.message !== message));
        }
        if (severity == 3) {
            setInfoArr(infoArr.filter(item => item.message !== message));
        }
    }



    useEffect(() => {
        api.start();
        setToggle(true);
    }, []);


    useEffect(() => {
        if (message && message.priority) {
            const payload = [...messageArr, message];
            setMessageArr(payload);
            if (message.priority == 1) {
                toast.error(message.message);
                setErrorArr([...errorArr, message]);

            }
            if (message.priority == 2) {
                toast.warning(message.message);
                setWarningArr([...warningArr, message]);
            }
            if (message.priority == 3) {
                toast.success(message.message);
                setInfoArr([...infoArr, message]);

            }
        }
    }, [message]);





    return (
        <div>

            <CustomGrid>
                <Button color="secondary" disabled={!toggle} onClick={handleClear}> Clear </Button>
                {!toggle
                    ? <Button onClick={handleStart} color="secondary" variant='outlined'>Start</Button>
                    : <Button onClick={handleStop} color="secondary" variant='outlined'>stop</Button>
                }
            </CustomGrid>



            <NotiBoard
                errorArr={errorArr}
                infoArr={infoArr}
                warningArr={warningArr}
                handleRemove={handleRemove}
            />


        </div>
    )
}

export default MessageList;




const CustomGrid = styled.div`
    display: flex;
    justify-content: center;
    margin:.3em;
    button {
        margin:.3em;
    }

    
`