import React from 'react';
import  {render, fireEvent} from "@testing-library/react";
import MessageList from './MessageList';
import NotiIttem from './NotiBoard/NotiIttem';
import { expect } from '@jest/globals';

it('Message list renders correctly', ()=>{
    const {queryByTestId, queryByPlaceholderName}= render(<MessageList/>);
    expect(queryByTestId('noti-board')).toBeTruthy();

});

describe('NotiItem CloseButton', ()=>{
    describe("Not touched", ()=>{
        it('does not trigger handleRemove NotiItem',()=>{
            const handleRemove = jest.fn();
            const {queryByTestId}= render(<NotiIttem handleRemove={handleRemove}/>);
            fireEvent.click(queryByTestId('noti-item'))
            expect(handleRemove).not.toHaveBeenCalled();
        })
    })
})