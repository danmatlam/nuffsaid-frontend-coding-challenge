

const MEDIA_SCREENS = {
    XS: {
        FROM: '0',
        TO: '500',
    },
    SMALL: {
        FROM: '500',
        TO: '768',
    },
    MEDIUM: {
        FROM: '768',
        TO: '4000',
    },
};

export {MEDIA_SCREENS}
